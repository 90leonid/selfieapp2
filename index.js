const fs = require('fs');
const Datastore = require('nedb');
const express = require('express');
const { response } = require('express');
const { request } = require('http');
const app = express();
app.listen(3000, () => console.log('listening at 3000'));
app.use(express.static('public'));
app.use(express.json({limit: '1mb'}));

const database = new Datastore('database.db');
database.loadDatabase();

app.post('/api', (request, response) => {
    console.log("!---- New Request ----!");
    const data = request.body;
    data.timestamp = Date.now();
    database.insert(data); 
    response.json(data);
});

app.get('/api', (request, response) => {
    database.find({}, (err, data) => {
      if(err) {
          response.end();
          return;
      }
      response.json(data); 
    });
});