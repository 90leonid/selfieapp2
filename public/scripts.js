//  setInterval(function() { getISS(api_url, marker, myMap); }, 1000);

function setup() {
  noCanvas();
  const video = createCapture(VIDEO);
  video.size(320, 240);
  settings(video);
};

let i = 0;
function settings(video) {
  const location = {
    latitude: 0,
    longitude: 0
  };
  checkGeolocation(location);
  const button = document.getElementById("bt01");
  button.addEventListener('click', async event => {
    const mood = document.getElementById("mood").value;
    document.getElementById("mood").value = "";
    video.loadPixels();
    const image64 = video.canvas.toDataURL();
    console.log(location);
    let lat = location.latitude;
    let lng = location.longitude;
    lat += i;
    i++;
    const data = { lat, lng, mood, image64 };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    const response = await fetch('/api', options);
    const responseData = await response.json();
    console.log(responseData);
    checkGeolocation(location);
  });
};




function checkGeolocation(location) {
  if('geolocation' in navigator) {
    console.log('geolocation available');
    navigator.geolocation.getCurrentPosition( (position) => {
      location.latitude = position.coords.latitude;
      location.longitude = position.coords.longitude;
      document.getElementById("latitude").textContent = location.latitude.toFixed(2);
      document.getElementById("longitude").textContent = location.longitude.toFixed(2);
    });
  } else {
      console.log('geolocation not available');
  }
}

function tryy() {
  return {latitude: 17, longitude: 18};
}




/*checkGeolocation(location);
const button = document.getElementById("bt01");
button.addEventListener('click', async event => {
  const mood = document.getElementById("mood").value;
  document.getElementById("mood").value = "";
  console.log(location)
  const lat = location.latitude;
  const lng = location.longitude;
  const data = { lat, lng, mood };
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  };
  const response = await fetch('/api', options);
  const responseData = await response.json();
  console.log(responseData);
})*/

