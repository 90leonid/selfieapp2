getData();

async function getData() {
    const response = await fetch('/api');
    const data = await response.json();
    console.log(data);
    for (item of data){
        const root = document.createElement('p');
        const geo = document.createElement('div');
        const date = document.createElement('div');
        const mood = document.createElement('div');
        const image = document.createElement('img');
        
        geo.textContent = `${item.lat}°, ${item.lng}°`;
        const dateString = new Date(item.timestamp).toLocaleString();
        date.textContent = dateString;
        mood.textContent = item.mood;
        image.src = item.image64;

        root.append(mood, geo, date, image);
        document.body.append(root);
        document.body.append(document.createElement('br'));
    }

}